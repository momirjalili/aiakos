# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-19 22:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_profile_oidc', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='profile',
            field=models.URLField(blank=True),
        ),
    ]
