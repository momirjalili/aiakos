from .auth import AuthView
from .oauth import OAuthDoneView
from .login_by_email import LoginByEmail
from .finish_registration_by_email import FinishRegistrationByEmail
from .identities import IdentitiesView
